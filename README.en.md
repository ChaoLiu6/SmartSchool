# SmartSchool

#### Description
校园帮APP是一个具有设备故障报备功能、失物招领通告功能、二手交易信息提供功能、各大考试报名途径的在校人员服务平台。校园帮APP是在研究了在校人员需求的基础上，应运而生的软件，它可以方便老师和学生的生活、可以节约师生在处理日常生活问题的时间、可以实现可用资源的二次利用，让用户的生活简单化。

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)